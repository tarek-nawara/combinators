package edu.parsec

import edu.parsec.parser.impl.Parser

/**
  * Application's entry point.
  *
  * @author Tarek
  */
object Main {
  def main(args: Array[String]): Unit = {
    val parser = Parser.parseChar('a')
    val res = parser.run(Stream('a', 'b', 'c'))
    res match {
      case Left(value) => println(s"parser failed, error: $value")
      case Right((value, remaining)) => println(s"parser succeed, value=$value, remaining=$remaining")
    }
  }
}
