package edu.parsec.parser.impl

import edu.parsec.parser.impl.Parser.{ParseResult, ParserFunc}
import edu.parsec.typeclass.Monad

/**
  * Implementation of a parser combinator.
  *
  * The parser type is a wrapper around a function
  * that will run over the input list to parse target structure.
  * if the parser managed to parse the input it will return `T` and
  * the remaining input, or a failure if it failed to parse the input.
  *
  * @tparam T object returned by the parser if it managed to parse
  *           the input list.
  * @author Tarek
  */
class Parser[T](private val parserFunc: ParserFunc[T]) extends Monad[Parser, T] {

  /** OR alias */
  def <|>[U <: T](parser: => Parser[U]): Parser[T] = or(parser)

  /**
    * OR two parsers together.
    *
    * If the first parser succeed we will return its result,
    * otherwise we return the second parser's result.
    *
    * @param other other parser to `Or` with.
    * @tparam U type of other parser.
    * @return parser resulting of `or` the two parsers
    */
  def or[U <: T](other: => Parser[U]): Parser[T] = {
    val innerFunc = (input: Stream[Char]) => {
      val resOne = this.run(input)
      resOne match {
        case Left(_) => other.run(input)
        case _ => resOne
      }
    }

    new Parser(innerFunc)
  }

  /** AND alias */
  def ~[U](other: => Parser[U]): Parser[(T, U)] = and(other)

  /**
    * And two parsers together.
    *
    * @param other the other parser to and with.
    * @tparam U type of other parser
    * @return parser composed of this and
    *         `other` parser
    */
  def and[U](other: => Parser[U]): Parser[(T, U)] = {
    this.liftM2(Tuple2.apply, other)
  }

  /** @inheritdoc*/
  override def pure[B](value: B): Parser[B] = {
    val innerFunc = (input: Stream[Char]) => Right((value, input))
    new Parser(innerFunc)
  }

  /** @inheritdoc*/
  override def apply[C](fM: Parser[T => C]): Parser[C] = {
    fM.flatMap(this.map)
  }

  /** @inheritdoc*/
  override def map[B](f: T => B): Parser[B] = {
    val innerFunc = (input: Stream[Char]) => {
      val res = this.run(input)
      res match {
        case Left(value) => Left(value)
        case Right((value, remaining)) => Right((f(value), remaining))
      }
    }

    new Parser(innerFunc)
  }

  /**
    * Run the parser over the given input.
    *
    * @param input input to parse
    * @return parsing result.
    */
  def run(input: Stream[Char]): ParseResult[T] = parserFunc(input)

  /** @inheritdoc*/
  override def flatMap[B](f: T => Parser[B]): Parser[B] = {
    val innerFunc = (input: Stream[Char]) => {
      val outerRes = this.run(input)
      outerRes match {
        case Left(value) => Left(value)
        case Right((value, remaining)) => f(value).run(remaining)
      }
    }

    new Parser(innerFunc)
  }
}

object Parser {
  type ParserFunc[A] = Stream[Char] => ParseResult[A]
  type ParseResult[A] = Either[String, (A, Stream[Char])]

  /**
    * Build a parser that when run will try to parse
    * the given character.
    *
    * @param c target character
    * @return parser to parse the target character.
    */
  def parseChar(c: Char): Parser[Char] = {
    val innerFunc = (input: Stream[Char]) => input match {
      case Stream.Empty => Left(s"Expected: $c, but reached end of string\n")
      case x #:: _ if x != c => Left(s"Expected: $c, but found $x\n")
      case x #:: xs if x == c => Right((c, xs))
    }

    new Parser(innerFunc)
  }
}
