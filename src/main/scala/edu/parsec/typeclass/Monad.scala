package edu.parsec.typeclass

import scala.language.higherKinds

/**
  * Interface Representing a `Monad`
  *
  * @tparam F Monadic class.
  * @tparam A Inner type of the Monadic class.
  * @author Tarek
  *
  */
trait Monad[F[_], A] {

  /**
    * Lift a value of any type into the monadic world.
    *
    * @param value value to lift to the monadic world.
    * @tparam B type of the value
    * @return Lifted value
    */
  def pure[B](value: B): F[B]

  /**
    * Map a non-monadic function over the monadic value.
    *
    * @param f non-monadic function to map.
    * @tparam B function's resulting type
    * @return new monad with the mapped value.
    */
  def map[B](f: A => B): F[B]

  /**
    * Apply the given function over the monad, then
    * flatten the result.
    *
    * @param f function used in the mapping.
    * @tparam B type of the resulting monad.
    * @return a new monad resulting from the function application.
    */
  def flatMap[B](f: A => F[B]): F[B]

  /**
    * Apply a function wrapped inside a monad over the current
    * monad.
    *
    * @param fM function wrapped inside a monad.
    * @tparam C type of the function's result
    * @return a new monad resulting from the function application.
    */
  def apply[C](fM: F[A => C]): F[C]

  /**
    * Sequentially compose this monad with another one, discarding
    * the value produced by this one.
    *
    * @param other other monad to compose.
    * @tparam B type of other monad.
    * @return a monad that will discard any results
    *         produced by this one.
    */
  def after[B](other: F[B]): F[B] = {
    this.flatMap(_ => other)
  }

  /**
    * Lift a function to monadic world. then apply it
    * to the current instance as first argument then to paramTwo
    *
    * @param f        function to lift
    * @param paramTwo second parameter for the function
    * @tparam B type of second parameter
    * @tparam C function's return type
    * @return a monadic value containing the result.
    */
  def liftM2[B, C](f: (A, B) => C, paramTwo: Monad[F, B]): F[C] = {
    val funcOne = pure(f.curried)
    val appOne = apply(funcOne)
    paramTwo.apply(appOne)
  }

}
