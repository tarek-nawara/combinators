package edu.parsec.parser.impl

import org.scalatest.FunSuite

class ParserSuite extends FunSuite {
  test("parsing single character should succeed") {
    val parser = Parser.parseChar('a')
    val res = parser.run(Stream('a', 'b', 'c'))
    assert(res.isRight)
    res match {
      case Left(_) => fail()
      case Right((value, remaining)) => assert(value == 'a' && remaining == Stream('b', 'c'))
    }
  }
}
